//
//  Constants.swift
//  TestApp
//
//  Created by Ruben Vidamo on 10/17/21.
//

struct Keys {
    static let lastVisitKey = "last_visited_date"
    static let lastScreen = "last_screen"
}

