//
//  File.swift
//  Test
//
//  Created by Ruben Vidamo on 10/11/21.
//

import Foundation

protocol Search {
    init(requestManager: RequestManager)
    func search(withTerm term: String, country: String, media: String, completion: ((Result<Data?, RequestError>)->())?)
}

class TrackService: Search {
    private let requestManager: RequestManager
    
    required init(requestManager: RequestManager = RequestManager(baseUrl: "https://itunes.apple.com/")) {
        self.requestManager = requestManager
    }

    /// Search itunes api
    /// - Parameters:
    ///   - term: API Key.
    ///   - country: Campaign Id.
    ///   - media: Identifer for referrer.
    func search(withTerm term: String, country: String, media: String, completion: ((Result<Data?, RequestError>) -> ())?) {
        
        requestManager.load(service: .search(term: term, country: country, media: media), headers: nil, payload: nil) { (result) in
            completion?(result)
        }
    }
}

