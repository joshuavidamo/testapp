//
//  ViewController.swift
//  TestApp
//
//  Created by Ruben Vidamo on 10/13/21.
//

import UIKit
import Kingfisher

class TrackTableViewController: UITableViewController, UISearchBarDelegate {
    
    // MARK: - Properties
    let lastVisit: String?
    
    let userDefaults = UserDefaults.standard
    
    var tracks = [Track]()
    
    lazy var filteredTracks = [Track]()
    
    let searchBar: UISearchBar = {
        let searchBar = UISearchBar()
        searchBar.searchBarStyle = UISearchBar.Style.default
        searchBar.placeholder = " Search"
        searchBar.sizeToFit()
        searchBar.isTranslucent = false
        searchBar.backgroundImage = UIImage()
        return searchBar
    }()
    
    // MARK: - View Lifecycle
    
    init(withDate lastVisit: String?) {
        self.lastVisit = lastVisit
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func loadView() {
        tableView = UITableView()
        tableView.backgroundColor = .white
        tableView.delegate = self
        tableView.dataSource     = self
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = "Track List"
        
        tableView.register(TrackTableViewCell.classForCoder(), forCellReuseIdentifier: "track")
        tableView.tableHeaderView = searchBar
        
        tableView.delegate = self
        tableView.dataSource = self
        searchBar.delegate = self
        tableView.refreshControl = UIRefreshControl()
        tableView.refreshControl?.addTarget(self, action: #selector(loadAPI), for: .valueChanged)

        loadAPI()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        if let savedTrack = userDefaults.object(forKey: Keys.lastScreen) as? Data {
            let decoder = JSONDecoder()
            if let track = try? decoder.decode(Track.self, from: savedTrack) {
                self.navigationController?.pushViewController(TrackDetailViewController(with: track), animated: true)
            }
        }
    }
    
    // MARK: - UITableViewDataSource
    
    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return searchBar.searchTextField.text!.isEmpty ? tracks.count : filteredTracks.count
    }
    
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 60
    }
    
    override func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let textView = UITextView()
        textView.isEditable = false
        textView.font = UIFont.systemFont(ofSize: 12)
        textView.textColor = .gray
        textView.textAlignment = .right
        textView.contentInset = UIEdgeInsets(top: 0, left: 10, bottom: 0, right: 10)
        textView.text = lastVisit != nil ? "Last Visited: \(lastVisit!)" : nil
        return textView
    }
    
    override func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return lastVisit != nil ? 26 : 0
    }
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "track", for: indexPath) as! TrackTableViewCell
        
        let track = searchBar.searchTextField.text!.isEmpty ? tracks[indexPath.row] : filteredTracks[indexPath.row]

        cell.imgView.kf.setImage(with: URL(string: track.artworkUrl100), placeholder: UIImage(named: "default"))
        cell.nameLabel.text = track.trackName
        cell.genreLabel.text = track.primaryGenreName
        cell.priceLabel.text = track.trackPrice != nil ? "$" + String(describing: track.trackPrice!) : "Free"
        cell.accessoryType = .disclosureIndicator
        
        return cell
    }
    
    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        let track = searchBar.searchTextField.text!.isEmpty ? tracks[indexPath.row] : filteredTracks[indexPath.row]
        self.navigationController?.pushViewController(TrackDetailViewController(with: track), animated: true)
    }
    
    // MARK: - UISearchBarDelegate
    
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        filteredTracks = tracks.filter({ $0.trackName?.contains(searchText) ?? false })
        self.tableView.reloadData()
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        searchBar.resignFirstResponder()
    }
    
    // MARK: -
    
    @objc func loadAPI() {
        let service = TrackService()
        
        service.search(withTerm: "star", country: "au", media: "movie") { result in
            DispatchQueue.main.async {
                self.tableView.refreshControl?.endRefreshing()
            
                switch result {
                case .success(let data):
                    guard let _data = data,
                          let response = try? JSONDecoder().decode(APIResponse.self, from: _data) else {
                        // log something
                        return
                    }
                    self.tracks = response.results
                    self.tableView.reloadData()
                case .failure(let err):
                    print(err)
                }
            }
        }
    }
}
