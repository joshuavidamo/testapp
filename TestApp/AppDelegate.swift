//
//  AppDelegate.swift
//  TestApp
//
//  Created by Ruben Vidamo on 10/13/21.
//

import UIKit

@main
class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        let userDefaults = UserDefaults.standard
        let lastVisit = userDefaults.string(forKey: Keys.lastVisitKey)
        
        let rootViewController = UINavigationController(rootViewController: TrackTableViewController(withDate: lastVisit))
        
        window = UIWindow()
        window?.rootViewController = rootViewController
        window?.makeKeyAndVisible()
        
        let date = Date()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMM dd YYYY h:mm a"
        
        userDefaults.setValue(dateFormatter.string(from: date), forKey: Keys.lastVisitKey)
        return true
    }
}

