//
//  RequestManager.swift
//  Test
//
//  Created by Ruben Vidamo on 10/11/21.
//

import Foundation

enum RequestMethod: String {
    case delete = "DELETE"
    case get = "GET" // we only need get for this challenge though
    case post = "POST"
    case put = "PUT"
}

public enum RequestError: Error, Equatable {
    case badURL
    case invalidHttpResponse
    case serializationFailed
    case genericError(response: Error)
    case upstreamError(data: Any?, response: HTTPURLResponse)
    
    public static func == (lhs: RequestError, rhs: RequestError) -> Bool {
        switch (lhs, rhs) {
        case (badURL, badURL):
            return true
        case (invalidHttpResponse, invalidHttpResponse):
            return true
        case (serializationFailed, serializationFailed):
            return true
        case (genericError(_), genericError(_)):
            return true
        case (upstreamError(_, _), upstreamError(_, _)):
            return true
        default:
            return false
        }
    }

}

final class RequestManager {
    
    private let baseUrl: String
    
    private let session: URLSessionProtocol
    
    init(baseUrl: String, session: URLSessionProtocol = URLSession.shared) {
        self.baseUrl = baseUrl
        self.session = session
    }
    
    func load(service: TrackEndpoint, headers: [String: String]?, payload: [String: Any]?, completion: @escaping (Result<Data?, RequestError>) -> ()) {
        
        guard !baseUrl.isEmpty, let url = URL(string: baseUrl + service.path) else {
            completion(.failure(.badURL))
            return
        }
        
        var urlRequest = URLRequest(url: url)
        urlRequest.httpMethod = service.method.rawValue
        if let _headers = headers {
            for (key, value) in _headers {
                urlRequest.setValue(value, forHTTPHeaderField: key)
            }
        }
        
        if let _payload = payload {
            guard let httpBody = try? JSONSerialization.data(withJSONObject: _payload) else {
                completion(.failure(.serializationFailed))
                return
            }
            urlRequest.httpBody = httpBody
        }
        
        session.dataTask(with: urlRequest) { (data, response, error) in
            if let _error = error {
                completion(.failure(.genericError(response: _error)))
                return
            }
            
            guard let response = response as? HTTPURLResponse,
                  let _data = data else {
                completion(.failure(.invalidHttpResponse))
               return
            }
            
            if 200 ... 299 ~= response.statusCode {
                completion(.success(_data))
            } else {
                completion(.failure(.upstreamError(data: _data, response: response)))
            }
        }.resume()
    }
}
