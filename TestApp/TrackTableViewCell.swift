//
//  TrackTableViewCell.swift
//  TestApp
//
//  Created by Ruben Vidamo on 10/14/21.
//

import UIKit

class TrackTableViewCell: UITableViewCell {
    
    // MARK: - Properties
    
    let genreLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 12)
        label.textColor = UIColor.gray
        label.translatesAutoresizingMaskIntoConstraints = false
        label.numberOfLines = 0
        label.lineBreakMode = .byWordWrapping
        return label
    }()
    
    let imgView: UIImageView =  {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.contentMode = .scaleAspectFill
        imageView.layer.masksToBounds = true
        return imageView
    }()
    
    let nameLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 14)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.numberOfLines = 0
        label.lineBreakMode = .byWordWrapping
        return label
    }()
    
    let priceLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 12)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.numberOfLines = 0
        label.lineBreakMode = .byWordWrapping
        label.textAlignment = .right
        return label
    }()
    
    // MARK: - View Lifecycle
    
    override init(style: UITableViewCell.CellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
        
        imgView.backgroundColor = UIColor.clear

        contentView.addSubview(imgView)
        contentView.addSubview(nameLabel)
        contentView.addSubview(genreLabel)
        contentView.addSubview(priceLabel)
        
        let imgViewConstraints = [
            imgView.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 8),
            imgView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 8),
            nameLabel.leadingAnchor.constraint(equalTo: imgView.trailingAnchor, constant: 8),
            contentView.bottomAnchor.constraint(equalTo: imgView.bottomAnchor, constant: 8),
            imgView.widthAnchor.constraint(equalTo: imgView.heightAnchor, multiplier: 1.0)
        ]
        
        let nameLabelConstraints = [
            nameLabel.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 8),
            priceLabel.leadingAnchor.constraint(equalTo: nameLabel.trailingAnchor, constant: 8),
        ]
        
        let genreLabelConstraints = [
            genreLabel.topAnchor.constraint(equalTo: nameLabel.bottomAnchor, constant: 8),
            genreLabel.leadingAnchor.constraint(equalTo: imgView.trailingAnchor, constant: 8),
            genreLabel.trailingAnchor.constraint(equalTo: priceLabel.leadingAnchor, constant: 8),
            contentView.bottomAnchor.constraint(equalTo: genreLabel.bottomAnchor, constant: 8),
        ]
        
        let priceLabelConstraints = [
            priceLabel.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 8),
            contentView.trailingAnchor.constraint(equalTo: priceLabel.trailingAnchor, constant: 8),
            contentView.bottomAnchor.constraint(equalTo: priceLabel.bottomAnchor, constant: 8)
        ]
        
        NSLayoutConstraint.activate(imgViewConstraints + nameLabelConstraints + genreLabelConstraints + priceLabelConstraints)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
