//
//  TrackDetailTableViewController.swift
//  TestApp
//
//  Created by Ruben Vidamo on 10/16/21.
//

import UIKit

class TrackDetailViewController: UIViewController {
    
    // MARK: - Properties
    
    let track: Track
    
    let userDefaults = UserDefaults.standard
    
    let contentView: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.clipsToBounds = true
        return view
    }()
    
    let descriptionLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 12)
        label.textColor = UIColor.darkGray
        label.translatesAutoresizingMaskIntoConstraints = false
        label.numberOfLines = 0
        label.lineBreakMode = .byWordWrapping
        return label
    }()
    
    let genreLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 12)
        label.textColor = UIColor.gray
        label.translatesAutoresizingMaskIntoConstraints = false
        label.numberOfLines = 0
        label.lineBreakMode = .byWordWrapping
        return label
    }()
    
    let imgView: UIImageView =  {
        let imageView = UIImageView()
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.contentMode = .scaleAspectFit
        imageView.layer.masksToBounds = true
        return imageView
    }()
    
    let nameLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 14)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.numberOfLines = 0
        label.lineBreakMode = .byWordWrapping
        return label
    }()
    
    let priceLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.systemFont(ofSize: 12)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.numberOfLines = 0
        label.lineBreakMode = .byWordWrapping
        label.textAlignment = .right
        return label
    }()
    
    let scrollView: UIScrollView = {
        let scrollView = UIScrollView()
        scrollView.translatesAutoresizingMaskIntoConstraints = false
        scrollView.clipsToBounds = true
        return scrollView
    }()
    
    // MARK: - View Lifecycle
    
    init(with track: Track) {
        self.track = track
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func loadView() {
        view = UIView()
        view.backgroundColor = .white
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        addViews()
        addConstraints()
        setValues()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        saveDetails()
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        userDefaults.removeObject(forKey: Keys.lastScreen)
    }
    
    func addViews() {
        view.addSubview(scrollView)
        scrollView.addSubview(contentView)
        contentView.addSubview(imgView)
        contentView.addSubview(nameLabel)
        contentView.addSubview(genreLabel)
        contentView.addSubview(priceLabel)
        contentView.addSubview(descriptionLabel)
    }
    
    func addConstraints() {
        
        let scrollViewConstraints = [
            scrollView.topAnchor.constraint(equalTo: view.topAnchor, constant: 0),
            scrollView.leadingAnchor.constraint(equalTo: view.leadingAnchor, constant: 0),
            scrollView.trailingAnchor.constraint(equalTo: view.trailingAnchor, constant: 0),
            scrollView.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: 0),
            scrollView.centerXAnchor.constraint(equalTo: view.centerXAnchor)
        ]
        
        let contentViewConstraints = [
            contentView.topAnchor.constraint(equalTo: scrollView.topAnchor, constant: 0),
            contentView.leadingAnchor.constraint(equalTo: scrollView.leadingAnchor, constant: 0),
            contentView.trailingAnchor.constraint(equalTo: scrollView.trailingAnchor, constant: 0),
            contentView.bottomAnchor.constraint(equalTo: scrollView.bottomAnchor, constant: 0),
            contentView.widthAnchor.constraint(equalTo: view.widthAnchor)
        ]
        
        let imgViewConstraints = [
            imgView.topAnchor.constraint(equalTo: contentView.topAnchor, constant: 8),
            imgView.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 8),
            contentView.trailingAnchor.constraint(equalTo: imgView.trailingAnchor, constant: 8),
            imgView.heightAnchor.constraint(equalToConstant: 100)
        ]
        
        let nameLabelConstraints = [
            nameLabel.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 8),
            nameLabel.topAnchor.constraint(equalTo: imgView.bottomAnchor, constant: 8),
            priceLabel.leadingAnchor.constraint(equalTo: nameLabel.trailingAnchor, constant: 8),
        ]

        let genreLabelConstraints = [
            genreLabel.topAnchor.constraint(equalTo: nameLabel.bottomAnchor, constant: 8),
            genreLabel.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 8),
            genreLabel.trailingAnchor.constraint(equalTo: priceLabel.leadingAnchor, constant: 8),
            descriptionLabel.topAnchor.constraint(equalTo: genreLabel.bottomAnchor, constant: 8)
        ]

        let priceLabelConstraints = [
            priceLabel.topAnchor.constraint(equalTo: imgView.bottomAnchor, constant: 8),
            contentView.trailingAnchor.constraint(equalTo: priceLabel.trailingAnchor, constant: 8),
            descriptionLabel.topAnchor.constraint(equalTo: priceLabel.bottomAnchor, constant: 8)
        ]

        let descriptionLabelConstratints = [
            descriptionLabel.leadingAnchor.constraint(equalTo: contentView.leadingAnchor, constant: 8),
            contentView.trailingAnchor.constraint(equalTo: descriptionLabel.trailingAnchor, constant: 8),
            contentView.bottomAnchor.constraint(equalTo: descriptionLabel.bottomAnchor, constant: 8)
        ]
        
        NSLayoutConstraint.activate(scrollViewConstraints
                                        + contentViewConstraints
                                        + imgViewConstraints
                                        + nameLabelConstraints
                                        + genreLabelConstraints
                                        + priceLabelConstraints
                                        + descriptionLabelConstratints
        )

    }
    
    func setValues() {
        imgView.kf.setImage(with: URL(string: track.artworkUrl100), placeholder: UIImage(named: "default"))
        nameLabel.text = track.trackName
        genreLabel.text = track.primaryGenreName
        priceLabel.text = track.trackPrice != nil ? "$" + String(describing: track.trackPrice!) : "Free"
        descriptionLabel.text = track.longDescription
    }
    
    func saveDetails() {
        let encoder = JSONEncoder()
        if let encoded = try? encoder.encode(track) {
            userDefaults.set(encoded, forKey: Keys.lastScreen)
        }
    }
}
