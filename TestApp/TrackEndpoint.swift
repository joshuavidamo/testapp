//
//  TrackEndpoint.swift
//  Test
//
//  Created by Ruben Vidamo on 10/11/21.
//

import Foundation

import Foundation

protocol Endpoint {

    /// RequestManager calls this method for the relative path to the endpoint
    ///
    /// - Returns: RELATIVE path of the endpoint
    var path: String { get }

    /// RequestManager calls this method for the http method
    ///
    /// - Returns: RequestMethod
    var method: RequestMethod { get }
}

/// Endpoint definition
enum TrackEndpoint: Endpoint {
    
    case search(term: String, country: String, media: String)
    
    /// Endpoint configuration
    var path: String {
        switch self {
        case .search(term: let term, country: let country, media: let media):
            return "/search?term=\(term)&country=\(country)&media=\(media)"
        }
    }
    
    var method: RequestMethod {
        let method: RequestMethod
        switch self {
        case .search:
            method = .get
        }
        return method
    }
}
