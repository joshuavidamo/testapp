//
//  APIResponse.swift
//  TestApp
//
//  Created by Ruben Vidamo on 10/15/21.
//

import Foundation

struct APIResponse: Codable {
    let resultCount: Int
    let results: [Track]
}
